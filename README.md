# PHP Instagram scraper

A simple PHP scraper to get instagram posts feed.

## Installation

To get the last posts of an account, first set a cron job ([cron-job.org](https://cron-job.org/ "cron-job.org")) on the `cron.php`.

Set on the cURL the `$username` variable with the instagram username and the `x-rapidapi-key` with an [Instagram's RapidAPI key](https://rapidapi.com/restyler/api/instagram40/ "rapidapi.com instagram40").

`cron.php` will put post's images in the `\img\` directory with a file format displaying datetime and post's shortcode (`DATETIME---SHORTCODE`);

In the `index.php` there is a simple loop function to get last posts sorted by datetime, using a limit, and including a link on the Instagram's post.