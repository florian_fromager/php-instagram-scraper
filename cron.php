<?php

    $username = 'YOUR_USERNAME';

    $curl = curl_init();

    curl_setopt_array( $curl, [
        
        CURLOPT_URL => "https://instagram40.p.rapidapi.com/proxy?url=https%3A%2F%2Fwww.instagram.com%2F$username%2F%3F__a%3D1",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => [
            
            "x-rapidapi-host: instagram40.p.rapidapi.com",
            "x-rapidapi-key: YOUR_RAPIDAPI_KEY"
            
        ],
        
    ]);

    $response = curl_exec( $curl );
    $err = curl_error( $curl );

    curl_close( $curl );

    if ( $err ) {

        echo "cURL Error #: $err";

    } else {

        $response = json_decode( $response );
        $posts = $response->graphql->user->edge_owner_to_timeline_media->edges;

        $i = 0;
        $limit = 6;

        foreach ( $posts as $post ) {

            if ( $i < $limit ) {

                $post = $post->node;
                $timestamp = strval( $post->taken_at_timestamp );
                $shortcode = $post->shortcode;
                $filename = "$timestamp---$shortcode";

                $filenameIn  = $post->thumbnail_src;
                $filenameOut = __DIR__ . '/img/' . basename( $filename ) . '.jpg';

                $contentOrFalseOnFailure   = file_get_contents( $filenameIn );
                $byteCountOrFalseOnFailure = file_put_contents( $filenameOut, $contentOrFalseOnFailure );

                $i++;

            }

        }

    }