<?php

    function get_string_between( $string, $start, $end ) {

        $string = " $string";

        $ini = strpos( $string, $start );
        if ( $ini == 0 ) return '';
        $ini += strlen( $start );

        $len = strpos( $string, $end, $ini ) - $ini;

        return substr( $string, $ini, $len );

    }

    $DIR = '/img/';
    
    if ( file_exists( $DIR ) && is_dir( $DIR ) ) {
        
        $scan_arr = scandir( $DIR );
        $files_arr = array_diff( $scan_arr, array( '.', '..' ) );
        arsort( $files_arr );
        
        foreach ( $files_arr as $filename ) {

            $shortcode = get_string_between( $filename, '---', '.jpg' );
            $url = "https://www.instagram.com/p/$shortcode/";

            echo "<a href='$url' target='_blank'><img src='/assets/instagram/$filename' alt='$filename'></a>";

        }

    } else {

        echo "Directory does not exist";

    }